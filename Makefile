NAME = talk
SOURCES = $(wildcard *.tex) $(wildcard tikz/*.tex) $(wildcard minted/*.tex)
DEPS = $(wildcard img/*.png)
V ?= 0

ifeq ("$(V)", "0")
	RUBBER_VERBOSE = -q
else ifeq ("$(V)", "1")
	RUBBER_VERBOSE =
else ifeq ("$(V)", "2")
	RUBBER_VERBOSE = -v
else ifeq ("$(V)", "3")
	RUBBER_VERBOSE = -vv
else ifeq ("$(V)", "4")
	RUBBER_VERBOSE = -vvv
else
# indenting here causes make to throw an unrelated error for some reason
$(error Invalid value for variable V, must be integer in range [0..4]!)
endif

RUBBER = rubber $(RUBBER_VERBOSE)

$(NAME).pdf: $(SOURCES) $(DEPS)
	$(RUBBER) --force --unsafe --pdf $(NAME)

.PHONY: o
o: $(NAME).pdf
	xdg-open $(NAME).pdf

.PHONY: spellcheck
spellcheck:
	for file in $(SOURCES); do \
		aspell -t -c $$file; \
	done

.PHONY: clean
clean:
	$(RUBBER) --clean $(NAME)

.PHONY: mrproper
mrproper: clean
	$(RM) $(NAME).pdf
